<?php

namespace app\modules\films\controllers;

use app\modules\films\models\Films;
use app\modules\films\models\FilmsSearch;
use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionAdd()
    {
        $model = new Films();

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('add', [
                'model' => $model,
            ]);
        }
    }

    public function actionIndex()
    {
        $searchModel = new FilmsSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTest1()
    {
        $post = \Yii::$app->request->post();
        $result = '';

        if (!empty($post)) {

            $n = $post['n'] + 0;
            $m = $post['m'] + 0;
            $i = $post['i'] + 0;
            $j = $post['j'] + 0;

            if ($i < $n && $j < $m && $i >= 0 && $j >= 0) {

                $result = max(
                    $n - $i - 1,
                    $i,
                    $j,
                    $m - $j - 1
                );
            } else {
                $result = "Некоректный ввод! Размер матрицы должен быть больше начальной точки и все данные должны быть натуральными числами";
            }
        }

        return $this->render('test1',
            [
                'result' => $result
            ]
        );
    }

    public function actionTest2()
    {
        $post = \Yii::$app->request->post();
        $array = [];

        if (!empty($post)) {

            $n = $post['n'] + 0;

            if ($n>=0) {

                $array = range(0, $n);
                array_walk($array, function (&$value, $key) {
                    $value = rand(0, 10000);
                });

            } else {
                $array = "Некоректный ввод! Размер матрицы должен быть натуральными числами";
            }
        }

        return $this->render('test2',
            [
                'result' => $array
            ]
        );
    }

}

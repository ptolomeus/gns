<?php

namespace app\modules\films\models;

use Yii;

/**
 * This is the model class for table "Films".
 *
 * @property integer $id
 * @property string $name
 * @property integer $year
 * @property integer $isActive
 */
class Films extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Films';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'isActive'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'year' => 'Year',
            'isActive' => 'Is Active',
        ];
    }
}

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\films\models\Films */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Задание 1';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="films-form">

    <? if($result!=='') {
         echo "<h2>Результат: $result</h2>";
    } ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= Html::Label('Количество строк'); ?>
    <br />
    <?= Html::textInput('n'); ?>
    <br />
    <?= Html::Label('Количество столбцов'); ?>
    <br />
    <?= Html::textInput('m') ?>
    <br />
    <?= Html::Label('Начальная точка(строка)'); ?>
    <br />
    <?= Html::textInput('i') ?>
    <br />
    <?= Html::Label('Начальная точка(столбец)'); ?>
    <br />
    <?= Html::textInput('j') ?>
    <br /><br />
    <div class="form-group">
        <?= Html::submitButton('Рассчитать', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

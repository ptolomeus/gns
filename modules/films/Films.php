<?php

namespace app\modules\films;

class Films extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\films\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\films\models\Films */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Задание 2';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="films-form">

    <? if (!empty($result)) {
        echo "<h2>Результат: </h2><pre>";
        print_r($result);
        echo "</pre>";
    } ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= Html::Label('Размер матрицы'); ?>
    <br/>
    <?= Html::textInput('n'); ?>

    <br/><br/>

    <div class="form-group">
        <?= Html::submitButton('Рассчитать', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

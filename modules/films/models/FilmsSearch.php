<?php

namespace app\modules\films\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\films\models\Films;

/**
 * FilmsSearch represents the model behind the search form about `app\modules\films\models\Films`.
 */
class FilmsSearch extends Films
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'year', 'isActive'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = Films::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andFilterWhere([
            'isActive' => true,
        ]);

        return $dataProvider;
    }
}
